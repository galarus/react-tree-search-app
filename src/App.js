import { Box, Text, Flex, Heading } from "rebass";
import { Label, Input } from "@rebass/forms";
import data from "./tree.json";
import { treeBFS } from "./tree.js"
import React from 'react';

const TreeContext = React.createContext();
const HighlightContext = React.createContext();

// react node rendering with props: title (string), depth (int) 
const NodeElement = ({ title, depth }) => {

  const tree = React.useContext(TreeContext);
  const kids = tree[title];
  const [highlighted, _] = React.useContext(HighlightContext);
  const isHightlighted = highlighted.includes(title);
  const [collapsed, setCollapsed] = React.useState(!isHightlighted);
  const baseHue = isHightlighted ? 60 : 100;
  const baseSat = 90 - depth * 6;
  const baseLit = 50 - depth * 4;
  const color = "hsl(" + baseHue + "," + baseSat + "%," + baseLit + "%)"
  const clickHandler = (e) => {
    // treeSearch();
    if (!isHightlighted) setCollapsed(!collapsed);
    e.stopPropagation()
  }
  return <Box onClick={clickHandler}
    width="50vw"
  >
    <Flex
      bg={color}
      p="0.2em"
      ml={depth + "em"}
    >
      <Box
      >
        <Text ml="0.2em">
          {title}
        </Text>
      </Box >
      {kids.length > 0 &&
        <Box ml="2em"
        >     {collapsed ? "+" : "-"}</Box>}
    </Flex>
    {(!collapsed || isHightlighted) &&
      kids.map(kid => <NodeElement title={kid} depth={depth + 1} />)}
  </Box>
}

const HeaderElement = () => {
  const [highlighted, setHighlighted] = React.useContext(HighlightContext);
  const tree = React.useContext(TreeContext)
  const [searchQuery, setSearchQuery] = React.useState("")
  const [searchError, setSearchError] = React.useState("")
  const handleInput = (e) => {
    const val = e.target.value;
    setSearchQuery(val);
    if (val === "") {
      setHighlighted([]);
      setSearchError("")
    }
    else if (tree[val] !== undefined) {
      setSearchError("")
      setHighlighted(treeBFS(tree, "a", val));
    } else {
      setHighlighted([]);
      setSearchError("no nodes found")
    }
  }
  return <Box m="1rem">
    <Heading>
      Tree search
    </Heading>
    <Label>type search query:</Label>
    <Input
      type="text"
      value={searchQuery}
      onChange={handleInput}
      width="20em"
    ></Input>
    <Text>resulting path: {searchError ? searchError : highlighted}</Text>
  </Box>
}


function App() {
  return (
    <TreeContext.Provider value={data}>
      <HighlightContext.Provider value={React.useState([])}>
        <Box >
          <HeaderElement />
          <NodeElement title="a" depth={1} />
        </Box>
      </HighlightContext.Provider>
    </TreeContext.Provider>
  );
}

export default App;
