import data from "./tree.json"


export function treeBFS(tree, start, end) {
    // setup BFS with first node path
    let queue = [];
    queue.push([start]);
    while (queue.length !== 0){
      // store first path in visited array
      let visited = queue.pop();
      // check if last visited node is the goal
      let currentNode = visited[visited.length -1];
      if (currentNode === end) return visited.flat(Infinity);
      // if not, add the next adjacent node paths to search queue
      tree[currentNode].forEach(adjacentNode => {
        let newPath =  [visited];
        newPath.push(adjacentNode);
        queue.push(newPath);
      })
    }
  }
  
  console.assert( 
      JSON.stringify(treeBFS(data, 'a', 'f')) === 
      JSON.stringify(["a", "c", "f"]), 
      "test failed");
  
